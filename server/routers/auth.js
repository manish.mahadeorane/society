const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const User = mongoose.model("User");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const JWT_SECERT = process.env.JWT_SECRET;
const requiredLogin = require("../middlewear/requiredLogin");

router.get("/", requiredLogin, (req, res) => {
  res.send("Home");
});

router.post("/signup", (req, res) => {
  const { name, email, password } = req.body;

  if (!name || !email || !password) {
    return res.status(422).json({ error: "Please enter all the fields" });
  }

  User.findOne({ email })
    .then((savedUser) => {
      if (savedUser) {
        return res.status(422).json({ error: "User already exist" });
      }

      bcrypt.hash(password, 12).then((hashedPassword) => {
        const user = new User({
          name,
          email,
          password: hashedPassword,
        });

        user
          .save()
          .then((user) => {
            res.json({ message: "User saved successfully!!" });
          })
          .catch((err) => {
            res.status(422).json({ error: err });
          });
      });
    })
    .catch((err) => {
      res.status(422).json({ error: err });
    });

  //   res.json({ message: "User added successfully" });
});

router.post("/signin", (req, res) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return res.status(422).json({ error: "Please enter all the fields" });
  }

  User.findOne({ email }).then((savedUser) => {
    if (!savedUser) {
      return res.status(422).json({ error: "Invalid username or password" });
    }

    bcrypt
      .compare(password, savedUser.password)
      .then((doMatch) => {
        if (doMatch) {
          //   return res.json({ message: "User logged in" });
          const token = jwt.sign({ _id: savedUser._id }, JWT_SECERT);
          const { _id, email, name } = savedUser;
          res.json({ token, user: { _id, email, name } });
        } else {
          return res
            .status(422)
            .json({ error: "Invalid username or password" });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  });
});

module.exports = router;
