const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const app = express();
const dotenv = require("dotenv");
dotenv.config();

require("./models/user");

const auth = require("./routers/auth");

const PORT = process.env.PORT || 2000;

mongoose.connect(process.env.MONGO_URL);

mongoose.connection.on("connected", () => {
  console.log("connected to db");
});

mongoose.connection.on("error", (err) => {
  console.log("DB error: " + err);
});

app.use(cors());
app.use(express.json());

app.use(auth);

app.listen(PORT, (req, res) => {
  console.log("Server started");
});
